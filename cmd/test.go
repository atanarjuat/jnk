/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"0xacab.org/atanarjuat/jnk/pkg/config"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// testCmd represents the test command
var testCmd = &cobra.Command{
	Use:   "test",
	Short: "A brief description of your command",
	Long:  `A longer description that spans multiple lines and likely contains examples`,
	Run: func(cmd *cobra.Command, args []string) {
		test()
	},
}

func test() {
	// quick things to test, delete me when done.
	cfg := config.NewConfigFromViper()
	fmt.Println("menshen port:", cfg.MenshenPort)
	fmt.Println("menshen enabled", viper.GetBool("menshen.enabled"))
	fmt.Println("openvpn enabled:", viper.GetBool("openvpn.enabled"))
	fmt.Println("bridge enabled:", viper.GetBool("bridge.enabled"))
	fmt.Println("bridge1 enabled:", viper.GetBool("bridge-1.enabled"))
	fmt.Println("bridge2 enabled:", viper.GetBool("bridge-2.enabled"))
}

func init() {
	rootCmd.AddCommand(testCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// testCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// testCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
