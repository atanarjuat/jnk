/*
Copyright © 2023 atanarjuat <atanarjuat@riseup.net>
*/
package cmd

import (
	"github.com/spf13/cobra"

	"0xacab.org/atanarjuat/jnk/pkg/docker"
)

// pullCmd represents the pull command
var pullCmd = &cobra.Command{
	Use:   "pull",
	Short: "Pull all needed images",
	Long:  `Docker pull for all the images needed for the system.`,
	Run: func(cmd *cobra.Command, args []string) {
		docker.PullImages()
	},
}

func init() {
	rootCmd.AddCommand(pullCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// pullCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// pullCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
