/*
Copyright © 2023 atanarjuat <atanarjuat@riseup.net>
*/
package cmd

import (
	"0xacab.org/atanarjuat/jnk/pkg/docker"
	"github.com/spf13/cobra"
)

// certCmd represents the cert command
var certCmd = &cobra.Command{
	Use:   "cert",
	Short: "Get a valid OpenVPN client config",
	Run: func(cmd *cobra.Command, args []string) {
		docker.GenerateClientCert()
	},
}

func init() {
	rootCmd.AddCommand(certCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// certCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// certCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
