package docker

import (
	"context"
	"fmt"

	"github.com/docker/docker/api/types/volume"
	"github.com/docker/docker/client"
)

var (
	vpnPersistentVolume = "leap-vpn-data"
)

func CreateVolume() {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	defer cli.Close()

	opts := volume.CreateOptions{
		Name: vpnPersistentVolume,
	}
	_, err = cli.VolumeCreate(ctx, opts)
	if err != nil {
		panic(err)
	}
	fmt.Println("create volume ok")
}
