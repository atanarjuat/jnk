package docker

import (
	"context"
	"fmt"
	"log"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

var (
	// Timeout for stopping/removing, in seconds.
	Timeout = 10
)

func StopContainer(containerName string, remove bool) {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

	// List all containers and find the one with the given name
	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}

	var containerID string
	for _, container := range containers {
		for _, name := range container.Names {
			if name == "/"+containerName { // Container names are prefixed with a slash
				containerID = container.ID
				break
			}
		}
		if containerID != "" {
			break
		}
	}

	if containerID == "" {
		log.Println("Container not found!")
		return
	}

	// Stop the container
	if err := cli.ContainerStop(ctx, containerID, container.StopOptions{Timeout: &Timeout}); err != nil {
		panic(err)
	}

	fmt.Printf("Container %s (name: %s) has been stopped\n", containerID, containerName)

	if remove {
		// Remove the container too
		err = cli.ContainerRemove(ctx, containerID, types.ContainerRemoveOptions{})
		if err != nil {
			panic(err)
		}
		fmt.Printf("Container %s has been removed\n", containerID)
	}
}
