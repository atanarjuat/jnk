package docker

import (
	"context"

	"0xacab.org/atanarjuat/jnk/pkg/config"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

func PullImages() {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	defer cli.Close()

	cfg := config.NewConfigFromViper()

	// Here we could be smarter and pick only what we know we're going to use (by looking at the config blocks).
	images := []string{
		cfg.ImageOpenVPN,
		cfg.ImageMenshen,
	}

	for _, img := range images {
		out, err := cli.ImagePull(ctx, img, types.ImagePullOptions{})
		if err != nil {
			panic(err)
		}

		defer out.Close()
		wrapStdout(out)

	}
}
