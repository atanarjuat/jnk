package docker

import (
	"bufio"
	"context"
	"io"
	"log"
	"os"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/moby/moby/pkg/stdcopy"
)

type CmdConfig struct {
	ContainerName string
	Detach        bool
	ExposedPorts  []string
	Interactive   bool
	Env           []string
	EnvFile       string
	NetAdmin      bool
	PortBindings  []string
	Silent        bool
}

// RunCmdInContainer executes the passed command in the specified container.
func RunCmdInContainer(img string, net *network.NetworkingConfig, cmd []string, cfg *CmdConfig, bufOut io.ReadWriter) {
	inout := make(chan []byte)
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	defer cli.Close()

	// Create network if it does not exist
	// List existing networks
	networks, err := cli.NetworkList(ctx, types.NetworkListOptions{})
	if err != nil {
		panic(err)
	}

	// Check if the network already exists
	netExists := false
	for _, network := range networks {
		if network.Name == NetworkName {
			netExists = true
			break
		}
	}

	// If the network doesn't exist, create it
	if !netExists {
		_, err = cli.NetworkCreate(ctx, NetworkName, types.NetworkCreate{})
		if err != nil {
			panic(err)
		}
		log.Printf("Network %s created\n", NetworkName)
	} else {
		log.Printf("Network %s already exists\n", NetworkName)
	}

	config := &container.Config{
		Image: img,
	}
	// try to get environment variables from envFile
	if cfg.EnvFile != "" {
		env, err := extractEnvVars(cfg.EnvFile)
		if err != nil {
			log.Println("Error:", err)
		} else {
			config.Env = env
			log.Println("Env:", env)
		}
	}

	// we append the env array that we may have been passed by the CmdConfig
	config.Env = append(config.Env, cfg.Env...)

	if cfg.ContainerName != "" {
		config.Hostname = cfg.ContainerName
	}

	if len(cmd) != 0 {
		config.Cmd = cmd
	}

	if cfg.Interactive {
		config.AttachStdin = true
		config.AttachStderr = true
		config.AttachStdout = true
		config.OpenStdin = true
		config.Tty = true
	}

	var exposedPorts = make(map[nat.Port]struct{})
	var portBindings = make(nat.PortMap)

	for _, port := range cfg.ExposedPorts {
		exposedPorts[nat.Port(port)] = struct{}{}
	}

	for _, binding := range cfg.PortBindings {
		parts := strings.Split(binding, ":")
		inContainer, inHost := parts[0], parts[1]

		log.Printf("Port bind: %s => %s", inContainer, inHost)

		exposedPorts[nat.Port(inContainer)] = struct{}{}
		portBindings[nat.Port(inContainer)] = []nat.PortBinding{
			{
				HostIP:   "0.0.0.0",
				HostPort: inHost,
			},
		}
	}

	hostConfig := &container.HostConfig{
		Mounts: []mount.Mount{
			{
				Type:   mount.TypeVolume,
				Source: datavolume,
				Target: "/etc/openvpn",
			},
		},
	}

	if len(portBindings) != 0 {
		hostConfig.PortBindings = portBindings
		config.ExposedPorts = exposedPorts
	}

	if cfg.NetAdmin {
		hostConfig.CapAdd = []string{"NET_ADMIN"}
	}

	resp, err := cli.ContainerCreate(
		ctx,
		config,
		hostConfig,
		net,
		nil, cfg.ContainerName)
	if err != nil {
		panic(err)
	}

	log.Println("Starting container")
	log.Println("ID", resp.ID)

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	if cfg.Detach {
		return
	}

	// we're attaching to the running container, so unless we specifically need something else,
	// I'm going to remove it on exit.
	defer func() {
		log.Println("Cleaning up")
		if err := cli.ContainerRemove(
			context.Background(),
			resp.ID,
			types.ContainerRemoveOptions{
				Force:         true,
				RemoveVolumes: false,
			},
		); err != nil {
			panic(err)
		}
	}()

	if cfg.Interactive || bufOut != nil {
		attachOptions := types.ContainerAttachOptions{
			Stdout: true,
			Stream: true,
		}
		if cfg.Interactive {
			attachOptions.Stderr = true
			attachOptions.Stdin = true
		}
		waiter, err := cli.ContainerAttach(ctx, resp.ID, attachOptions)

		if !cfg.Silent {
			go io.Copy(os.Stdout, waiter.Reader)
			go io.Copy(os.Stderr, waiter.Reader)
		}

		if bufOut != nil {
			_, err = io.Copy(bufOut, waiter.Reader)
			if err != nil {
				log.Println("error copy", err)
			}
		}

		if err != nil {
			panic(err)
		}

		go func() {
			scanner := bufio.NewScanner(os.Stdin)
			for scanner.Scan() {
				inout <- []byte(scanner.Text())
			}
		}()

		// Write to docker container
		go func(w io.WriteCloser) {
			for {
				data, ok := <-inout
				if !ok {
					w.Close()
					return
				}

				w.Write(append(data, '\n'))
			}
		}(waiter.Conn)
	}

	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			panic(err)
		}
	case <-statusCh:
	}

	out := GetLogs(ctx, cli, resp.ID)
	if !cfg.Silent {
		stdcopy.StdCopy(os.Stdout, os.Stderr, out)
	}
}

// extractEnvVars reads environment variables from a file.
func extractEnvVars(envFile string) ([]string, error) {
	var env []string

	file, err := os.Open(envFile)
	if err != nil {
		return env, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" && !strings.HasPrefix(line, "#") {
			env = append(env, line)
		}
	}

	if err := scanner.Err(); err != nil {
		return env, err
	}
	return env, nil
}

// GetLogs return logs from the container io.ReadCloser. It's the caller duty
// to do a stdcopy.StdCopy. Any other method might render unknown
// unicode character as log output has both stdout and stderr. That starting
// has info if that line is stderr or stdout.
func GetLogs(ctx context.Context, cli *client.Client, contName string) (logOutput io.ReadCloser) {
	options := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
	}

	out, err := cli.ContainerLogs(ctx, contName, options)
	if err != nil {
		panic(err)
	}

	return out
}
