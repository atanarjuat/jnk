# jnk (جنك)

![junk](junk.jpg)

A small utility to quickly configure OpenVPN services. Intended for development and testing.

## Configuration

You can pass an ini-style config file with the `--config` flag.

In the `[images]` section you can override which images are used for each
service (by default it will use the latest in 0xacab registry for each
project).

* The `menshen` block is mandatory, since it's the main port that we will expose.
* The `openvpn` block is optional. If it is enabled, it will run an OpenVPN container (TODO: auto-configure menshen to read vars from the control port).
* The `bridge-x` blocks are optional. It will run as many `obfsvpn` containers as needed for each type configured.

```ini
[images]
menshen = menshen-test
obfsvpn = obfsvpn-test
openvpn =

[menshen]
enabled = true
auto-tls = false
from-eip-url = https://black.riseup.net/3/config/eip-service.json
allow-gateway-list = true
allow-bridge-list = true
bridges = bridge-1:9090

[openvpn]
enabled = false
addr = localhost:1194

[bridge-1]
enabled = true
type = obfs4
gateway = 163.172.211.109:1194
location = amsterdam
addr = localhost:4430

[bridge-2]
enabled = false
type = obfs4-kcp
gateway = 163.172.211.109:1194
location = amsterdam
addr = localhost:4431
```
